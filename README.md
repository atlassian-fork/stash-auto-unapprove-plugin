Bitbucket Server Auto Unapprove Pull Request Plugin
===================================================

Adds a pull request setting automatically marking reviewers as unapproved when a pull request is updated.

Minimum Requirements
--------------------

* Java 8
* Apache Maven 3.5+
* Atlassian Plugin SDK 8


Building the plugin
-------------------

To build it using the Atlassian Plugin SDK

    atlas-package


Running the plugin in Bitbucket Server
--------------------------------------

To run the plugin using Bitbucket Server in debug mode

    atlas-debug


Running the tests
-----------------

To run the unit tests

    atlas-unit-test
    

Quick Reload
------------

For fast plugin reloads during development, the [Quick Reload](https://bitbucket.org/atlassianlabs/quickreload)
plugin is used. After starting Bitbucket Server with `atlas-debug` or `mvn bitbucket:debug` quick reload can be triggered
by running `atlas-package` or `mvn package` in another terminal window. Quick Reload will detect the updated JAR file
and upload it to the running Bitbucket Server instance.

Quick Reload by default is rather silent and it is not obvious from the logs when the reload has actually completed.
Enabling INFO level logging for the Quick Reload loggers can be done like so:

    curl -u admin -v -X PUT -d "" -H "Content-Type: application/json" \
    http://localhost:7990/bitbucket/rest/api/latest/logs/logger/com.atlassian.labs.plugins.quickreload/info

After this is enabled, each time a reload completes the message "Quick Reload Finished" will be logged.
