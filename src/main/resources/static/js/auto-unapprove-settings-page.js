define('stash-auto-unapprove-plugin/auto-unapprove-settings-page', [
    'jquery',
    'exports',
], function($, exports) {
    'use strict';

    function toggleDisabled(shouldDisable) {
        const elements = document.querySelectorAll('.autounapprove-settings-content input');
        for (var i = 0; i < elements.length; i++) {
            elements[i].disabled = shouldDisable;
            elements[i].setAttribute('aria-disabled', shouldDisable);
        }
    }

    exports.onReady = function(useCustom) {
        toggleDisabled(!useCustom);
        $('.inherit-settings-toggle').on('change', function(e) {
            if ('INHERIT' === e.target.value) {
                e.target.checked = false;
                // TODO: Once compatibility for 5.x is removed, require @atlassian/aui as AJS instead of using the global AJS
                const dialog = AJS.dialog2(bitbucket.internal.autounapprove.settings.dialog());
                dialog.show();
                dialog.$el.on('click', '.confirm-button', function() {
                    e.target.checked = true;
                    toggleDisabled(true);
                    $('#auto-unapprove-settings-form').submit();
                });
                dialog.$el.on('click', '.cancel-button', function() {
                    $('#inherit-settings-selection-custom').attr('checked', true);
                    dialog.hide();
                });
            } else {
                toggleDisabled(false);
            }
        });
    };

});
