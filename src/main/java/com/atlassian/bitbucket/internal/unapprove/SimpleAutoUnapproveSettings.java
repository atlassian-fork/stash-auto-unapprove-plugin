package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettings;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class SimpleAutoUnapproveSettings implements AutoUnapproveSettings {

    private final boolean enabled;
    private final Scope scope;

    private SimpleAutoUnapproveSettings(Builder builder) {
        enabled = builder.enabled;
        scope = builder.scope;
    }

    @Nonnull
    public static Builder builder(@Nonnull Scope scope) {
        return new Builder(scope);
    }

    @Nonnull
    @Override
    public Scope getScope() {
        return scope;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public static final class Builder {

        private final Scope scope;
        private boolean enabled;

        private Builder(@Nonnull Scope scope) {
            this.scope = requireNonNull(scope, "scope");
        }

        @Nonnull
        public AutoUnapproveSettings build() {
            return new SimpleAutoUnapproveSettings(this);
        }

        @Nonnull
        public Builder enabled(boolean value) {
            enabled = value;
            return this;
        }
    }
}
