package com.atlassian.bitbucket.internal.unapprove.web;

import com.atlassian.bitbucket.AuthorisationException;
import com.atlassian.bitbucket.internal.unapprove.SimpleAutoUnapproveSettings;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.scope.*;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettings;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class AutoUnapproveSettingsServlet extends HttpServlet {

    private static final String PARAM_INHERIT_URL = "inheritUrl";
    private static final String PARAM_UNAPPROVE_ON_UPDATE = "enabled";
    private static final String PARAM_USE_CUSTOM = "useCustom";
    private static final String PLUGIN_KEY = "com.atlassian.stash.plugin.stash-auto-unapprove-plugin";
    private static final String REPOSITORY_SETTINGS_PAGE_CONTEXT = "bitbucket.page.autounapprove.settings";
    private static final String RESOURCE_KEY = PLUGIN_KEY + ":auto-unapprove-settings-page-templates";
    private static final String REST_PATH = "autounapprove";
    private static final String TEMPLATE_KEY = "bitbucket.internal.autounapprove.settings.page.settingsPage";

    private final AutoUnapproveService autoUnapproveService;
    private final NavBuilder navBuilder;
    private final PageBuilderService pageBuilderService;
    private final PermissionService permissionService;
    private final PermissionValidationService permissionValidationService;
    private final ProjectService projectService;
    private final RepositoryService repositoryService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final XsrfTokenAccessor xsrfTokenAccessor;
    private final XsrfTokenValidator xsrfTokenValidator;

    public AutoUnapproveSettingsServlet(PageBuilderService pageBuilderService, NavBuilder navBuilder,
                                        PermissionService permissionService, PermissionValidationService permissionValidationService,
                                        ProjectService projectService, RepositoryService repositoryService,
                                        SoyTemplateRenderer soyTemplateRenderer,
                                        AutoUnapproveService autoUnapproveService, XsrfTokenAccessor xsrfTokenAccessor,
                                        XsrfTokenValidator xsrfTokenValidator) {
        this.pageBuilderService = pageBuilderService;
        this.navBuilder = navBuilder;
        this.permissionService = permissionService;
        this.permissionValidationService = permissionValidationService;
        this.repositoryService = repositoryService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.projectService = projectService;
        this.autoUnapproveService = autoUnapproveService;
        this.xsrfTokenAccessor = xsrfTokenAccessor;
        this.xsrfTokenValidator = xsrfTokenValidator;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Scope scope = getScope(request.getPathInfo());
        if (scope == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        ImmutableMap.Builder<String, Object> contextBuilder = ImmutableMap.builder();
        scope.accept(new ScopeVisitor<Void>() {

            @Override
            public Void visit(@Nonnull ProjectScope scope) {
                contextBuilder.put("project", scope.getProject());
                if (permissionService.hasGlobalPermission(Permission.ADMIN)) {
                    contextBuilder.put(PARAM_INHERIT_URL, navBuilder.pluginServlets().path(REST_PATH).buildRelative());
                }
                return null;
            }

            @Override
            public Void visit(@Nonnull RepositoryScope scope) {
                Project project = scope.getProject();
                contextBuilder.put("project", project);
                contextBuilder.put("repository", scope.getRepository());
                if (permissionService.hasProjectPermission(project, Permission.PROJECT_ADMIN)) {
                    contextBuilder.put(PARAM_INHERIT_URL, navBuilder.pluginServlets().path(REST_PATH, "projects",
                            project.getKey()).buildRelative());
                }
                return null;
            }
        });
        AutoUnapproveSettings settings = autoUnapproveService.getSettings(scope);
        contextBuilder.put(PARAM_USE_CUSTOM, settings.getScope().equals(scope));
        contextBuilder.put(PARAM_UNAPPROVE_ON_UPDATE, settings.isEnabled());
        contextBuilder.putAll(getXsrfData(request, response));

        prepareResponse(response);
        try {
            soyTemplateRenderer.render(response.getWriter(), RESOURCE_KEY, TEMPLATE_KEY, contextBuilder.build());
        } catch (SoyException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        permissionValidationService.validateAuthenticated();

        if (!xsrfTokenValidator.validateFormEncodedToken(request)) {
            request.getRequestDispatcher(navBuilder.xsrfNotification().buildRelNoContext()).forward(request, response);
            return;
        }

        Scope scope = getScope(request.getPathInfo());
        if (scope == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        boolean inheritSettings = "INHERIT".equals(request.getParameter(PARAM_USE_CUSTOM));
        if (inheritSettings) {
            autoUnapproveService.removeSettings(scope);
        } else {
            autoUnapproveService.setSettings(SimpleAutoUnapproveSettings.builder(scope)
                    .enabled(Boolean.parseBoolean(request.getParameter(PARAM_UNAPPROVE_ON_UPDATE)))
                    .build());
        }

        prepareResponse(response);
        response.sendRedirect(request.getRequestURI());
    }

    private Scope getProjectScope(String projectKey) {
        if (isBlank(projectKey)) {
            return null;
        }
        try {
            Project project = projectService.getByKey(projectKey);
            return project == null ? null : Scopes.project(project);
        } catch (AuthorisationException e) {
            return null;
        }
    }

    private Scope getRepositoryScope(String projectKey, String repositorySlug) {
        if (isBlank(projectKey) || isBlank(repositorySlug)) {
            return null;
        }
        try {
            Repository repository = repositoryService.getBySlug(projectKey, repositorySlug);
            return repository == null ? null : Scopes.repository(repository);
        } catch (AuthorisationException e) {
            return null;
        }
    }

    @Nullable
    private Scope getScope(String pathInfo) {
        if (isBlank(pathInfo)) {
            return null;
        }

        String[] pathParts = pathInfo.substring(1).split("/");
        switch (pathParts.length) {
            case 1:
                return Scopes.global();
            case 3:
                return getProjectScope(pathParts[2]);
            case 5:
                return getRepositoryScope(pathParts[2], pathParts[4]);
            default:
                return null;
        }
    }

    private Map<String, Object> getXsrfData(HttpServletRequest req, HttpServletResponse resp) {
        return ImmutableMap.of("xsrfTokenName", xsrfTokenValidator.getXsrfParameterName(),
                "xsrfTokenValue", xsrfTokenAccessor.getXsrfToken(req, resp, true));
    }

    private void prepareResponse(ServletResponse response) {
        pageBuilderService.assembler().resources().requireContext(REPOSITORY_SETTINGS_PAGE_CONTEXT);
        response.setContentType("text/html;charset=UTF-8");
    }
}
