package com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.internal.unapprove.SimpleAutoUnapproveSettings;
import com.atlassian.bitbucket.rest.BadRequestException;
import com.atlassian.bitbucket.rest.util.ResponseFactory;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AbstractAutoUnapproveSettingsResource {

    private final AutoUnapproveService unapproveService;

    public AbstractAutoUnapproveSettingsResource(AutoUnapproveService unapproveService) {
        this.unapproveService = unapproveService;
    }

    @DELETE
    public Response deleteSettings(@Context Scope scope) {
        return ResponseFactory.ok(unapproveService.removeSettings(scope)).build();
    }

    @GET
    public Response getSettings(@Context Scope scope) {
        return ResponseFactory.ok(new RestAutoUnapproveSettings(unapproveService.getSettings(scope))).build();
    }

    @POST
    public Response setSettings(@Context Scope scope, RestAutoUnapproveSettings settings) {
        if (settings.getScope() != null && !isEqualScopes(scope, settings.getScope())) {
            throw new BadRequestException("bitbucket.web.autounapprove.settings.rest.scopes.mismatched");
        }
        unapproveService.setSettings(SimpleAutoUnapproveSettings.builder(scope)
                .enabled(settings.enabled())
                .build());
        return getSettings(scope);
    }

    private static boolean isEqualScopes(Scope scope1, Scope scope2) {
        return Objects.equals(scope1.getResourceId().orElse(null), scope2.getResourceId().orElse(null));
    }
}
