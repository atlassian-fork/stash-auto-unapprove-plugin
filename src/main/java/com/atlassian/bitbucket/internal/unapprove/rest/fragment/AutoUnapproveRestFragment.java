package com.atlassian.bitbucket.internal.unapprove.rest.fragment;

import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.rest.RestErrorMessage;
import com.atlassian.bitbucket.rest.fragment.RestFragment;
import com.atlassian.bitbucket.rest.fragment.RestFragmentContext;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Optional;

public class AutoUnapproveRestFragment implements RestFragment {

    static final String AUTO_UNAPPROVE_KEY = "unapproveOnUpdate";

    private final I18nService i18nService;
    private final AutoUnapproveService unapproveService;

    public AutoUnapproveRestFragment(I18nService i18nService, AutoUnapproveService unapproveService) {
        this.i18nService = i18nService;
        this.unapproveService = unapproveService;
    }

    @Nonnull
    @Override
    public Map<String, Object> execute(@Nonnull RestFragmentContext fragmentContext,
                                       @Nonnull Map<String, Object> requestContext) {
        Repository repository = (Repository) requestContext.get("repository");

        if ("GET".equals(fragmentContext.getMethod())) {
            return doGet(repository);
        }
        if ("POST".equals(fragmentContext.getMethod())) {
            return doPost(fragmentContext, repository);
        }
        return ImmutableMap.of(AUTO_UNAPPROVE_KEY, i18nService.getMessage("bitbucket.web.autounapprove.rest.method.unsupported",
                fragmentContext.getMethod()));
    }

    @Nonnull
    @Override
    public Map<String, Object> validate(@Nonnull RestFragmentContext fragmentContext,
                                        @Nonnull Map<String, Object> requestContext) {
        if ("POST".equals(fragmentContext.getMethod())) {
            Optional<Object> maybeProperty = fragmentContext.getBodyProperty(AUTO_UNAPPROVE_KEY);
            if (maybeProperty.isPresent() && !(maybeProperty.get() instanceof Boolean)) {
                return new RestErrorMessage(AUTO_UNAPPROVE_KEY,
                        i18nService.getMessage("bitbucket.web.autounapprove.rest.invalid"));
            }
        }

        return ImmutableMap.of();
    }

    private Map<String, Object> doGet(Repository repository) {
        return ImmutableMap.of(AUTO_UNAPPROVE_KEY, unapproveService.isEnabled(repository));
    }

    private Map<String, Object> doPost(RestFragmentContext fragmentContext, Repository repository) {
        fragmentContext.getBodyProperty(AUTO_UNAPPROVE_KEY)
                .ifPresent(property -> unapproveService.setEnabled(repository, (Boolean) property));

        return doGet(repository);
    }
}
