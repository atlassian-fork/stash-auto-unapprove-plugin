package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;

import javax.annotation.Nonnull;

public interface InternalAutoUnapproveService extends AutoUnapproveService {

    void withdrawApprovals(@Nonnull PullRequest pullRequest);
}
