package com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Path;

@AnonymousAllowed
@Path("settings")
@Singleton
public class AutoUnapproveGlobalSettingsResource extends AbstractAutoUnapproveSettingsResource {

    public AutoUnapproveGlobalSettingsResource(AutoUnapproveService unapproveService) {
        super(unapproveService);
    }
}
