package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.event.project.ProjectDeletedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestReopenedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestRescopedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestUpdatedEvent;
import com.atlassian.bitbucket.event.repository.RepositoryDeletedEvent;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.repository.Ref;
import com.atlassian.bitbucket.scope.Scopes;
import com.atlassian.event.api.EventListener;

import java.util.concurrent.ExecutorService;

/**
 * Listens for events which are relevant to unapprove processing.
 *
 * @since 2.2
 */
public class AutoUnapproveListener {

    private final RescopeAnalyzer analyzer;
    private final ExecutorService executorService;
    private final InternalAutoUnapproveService unapproveService;

    public AutoUnapproveListener(RescopeAnalyzer analyzer, ExecutorService executorService,
                                 InternalAutoUnapproveService unapproveService) {
        this.analyzer = analyzer;
        this.executorService = executorService;
        this.unapproveService = unapproveService;
    }

    @EventListener
    public void onProjectDeleted(ProjectDeletedEvent event) {
        unapproveService.removeSettings(Scopes.project(event.getProject()));
    }

    /**
     * Withdraws approvals when a declined pull request is reopened, to ensure reviewers verify the current changes
     * before it is merged.
     * <p>
     * When a pull request is reopened, it may be {@link #onPullRequestRescoped rescoped} as part of being opened,
     * so the last-approved changes may not match the reopened diff.
     * <p>
     * Starting in Bitbucket Server 4.5, it is possible for a pull request to rescoped one or more times <i>and
     * then declined</i> in a single database transaction. When that happens, {@link #onPullRequestRescoped} is
     * not able to withdraw approvals (because the pull request is no longer open when the event is raised), so
     * ensuring approvals are withdrawn can <i>only</i> be done when the pull request is reopened.
     *
     * @param event describes which pull request was reopened
     */
    @EventListener
    public void onPullRequestReopened(PullRequestReopenedEvent event) {
        PullRequest pullRequest = event.getPullRequest();
        if (unapproveService.isEnabled(pullRequest)) {
            unapproveService.withdrawApprovals(pullRequest);
        }
    }

    /**
     * Determines whether the rescope has updated the pull request's diff. If rescoping does not change the pull
     * request's diff, approvals should not be reset.
     *
     * @param event describes which pull request was rescoped, and how its refs were updated
     */
    @EventListener
    public void onPullRequestRescoped(PullRequestRescopedEvent event) {
        PullRequest pullRequest = event.getPullRequest();
        if (pullRequest.isOpen() && unapproveService.isEnabled(pullRequest)) {
            executorService.execute(() -> {
                if (analyzer.isUpdated(pullRequest, event.getPreviousFromHash())) {
                    unapproveService.withdrawApprovals(pullRequest);
                }
            });
        }
    }

    /**
     * Determines whether the updated pull request's target branch has changed, and whether the new branch refers
     * to a different commit than the previous one. If the target branches both refer to the same commit, approvals
     * should not be reset.
     *
     * @param event describes which pull request was updated and how
     */
    @EventListener
    public void onPullRequestUpdated(PullRequestUpdatedEvent event) {
        PullRequest pullRequest = event.getPullRequest();
        Ref previousTo = event.getPreviousToBranch();
        if (previousTo != null && pullRequest.isOpen() && unapproveService.isEnabled(pullRequest) &&
                !pullRequest.getToRef().getLatestCommit().equals(previousTo.getLatestCommit())) {
            unapproveService.withdrawApprovals(pullRequest);
        }
    }

    @EventListener
    public void onRepositoryDeleted(RepositoryDeletedEvent event) {
        unapproveService.removeSettings(Scopes.repository(event.getRepository()));
    }
}
