package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.pull.PullRequest;

import javax.annotation.Nonnull;

public interface RescopeAnalyzer {

    boolean isUpdated(@Nonnull PullRequest pullRequest, @Nonnull String previousFrom);
}
