package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.io.LineReader;
import com.atlassian.bitbucket.io.LineReaderOutputHandler;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandInputHandler;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.bitbucket.scm.git.GitScm;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.atlassian.bitbucket.throttle.ResourceBusyException;
import com.atlassian.bitbucket.throttle.ThrottleService;
import com.atlassian.bitbucket.throttle.Ticket;
import com.atlassian.bitbucket.util.IoUtils;
import com.atlassian.bitbucket.util.Timer;
import com.atlassian.bitbucket.util.TimerUtils;
import com.atlassian.utils.process.BaseInputHandler;
import com.atlassian.utils.process.BaseOutputHandler;
import com.atlassian.utils.process.ProcessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static com.atlassian.bitbucket.util.MoreCollectors.toImmutableList;

public class GitRescopeAnalyzer implements RescopeAnalyzer {

    static final String RESOURCE = "scm-command";

    private static final Logger log = LoggerFactory.getLogger(GitRescopeAnalyzer.class);

    private final GitCommandBuilderFactory builderFactory;
    private final ThrottleService throttleService;
    private final AutoUnapproveConfig unapproveConfig;

    public GitRescopeAnalyzer(GitCommandBuilderFactory builderFactory, ThrottleService throttleService,
                              AutoUnapproveConfig unapproveConfig) {
        this.builderFactory = builderFactory;
        this.throttleService = throttleService;
        this.unapproveConfig = unapproveConfig;
    }

    @Override
    public boolean isUpdated(@Nonnull PullRequest pullRequest, @Nonnull String previousFrom) {
        PullRequestRef toRef = pullRequest.getToRef();
        Repository repository = toRef.getRepository();

        PullRequestRef fromRef = pullRequest.getFromRef();
        String currentFrom = fromRef.getLatestCommit();
        if (previousFrom.equals(currentFrom)) {
            return false;
        }

        // For Git repositories, use "git diff a...b | git patch-id --stable" to determine whether
        // the pull request's diff has been updated in a meaningful way
        if (GitScm.ID.equals(repository.getScmId())) {
            //noinspection unused
            try (Timer timer = TimerUtils.start("Calculate patch IDs");
                 Ticket ticket = throttleService.acquireTicket(RESOURCE)) {
                List<String> oldPatchIds = calculatePatchIds(pullRequest, previousFrom);
                if (oldPatchIds == null) {
                    // If we can't generate patch ID(s) for the previous diff, don't bother trying
                    // to generate them for the current diff.
                    // The most likely reason patch ID(s) can't be generated is that the commits
                    // produce an empty diff. Two empty diffs shouldn't be considered equivalent,
                    // even though they're the "same"
                    return true;
                }
                List<String> newPatchIds = calculatePatchIds(pullRequest, currentFrom);

                log.debug("Old patch ID(s): {}; new patch ID(s): {}", oldPatchIds, newPatchIds);
                return !oldPatchIds.equals(newPatchIds);
            } catch (ResourceBusyException e) {
                log.warn("Could not acquire a [{}] ticket to calculate patch-ids", e.getResourceName());
                // Fall-through intentional
            } catch (Exception e) {
                log.warn("Failed to calculate patch-ids", e);
                // Fall-through intentional
            }
        }

        // previousFrom and currentFrom don't match, and we couldn't check patch-ids, so, since the
        // from commit has changed, consider the pull request updated
        return true;
    }


    // Credit (and kudos!) for this approach to determining whether a pull request's diff has been updated
    // goes to G. Sylvie Davies. Thanks for your contribution!
    private List<String> calculatePatchIds(PullRequest pullRequest, String fromHash) {
        String toHash = pullRequest.getToRef().getLatestCommit();
        Repository toRepo = pullRequest.getToRef().getRepository();
        Repository fromRepo = pullRequest.getFromRef().getRepository();

        GitScmCommandBuilder diffBuilder = builderFactory.builder(toRepo).command("diff")
                //Credit (and kudos!) to Troy Germain for improved binary handling. Thanks for your contribution!
                .argument("--binary") // Output a diff for binary files to detect binary file-only changes
                .argument("--full-index") // Implied by --binary; passed explicitly for completeness
                .argument(toHash + "..." + fromHash)
                .argument("--");
        if (pullRequest.isCrossRepository()) {
            diffBuilder.alternates(Collections.singleton(fromRepo));
        }

        try (TmpFileOutputHandler outputHandler = new TmpFileOutputHandler(unapproveConfig.getTempDir());
             Timer ignored = TimerUtils.start("Calculate patch ID for " + toHash + "..." + fromHash)) {
            File diffFile = diffBuilder.build(outputHandler).call();
            if (diffFile == null) {
                // git diff ran without error, but produced no diff. This means the commits have identical
                // contents. Return null to indicate no patch ID could be generated
                return null;
            }

            // If we make it here, we _know_ we have non-empty diff output. Given diff output, we should be able
            // to get one or more valid patch IDs
            return builderFactory.builder(toRepo).command("patch-id")
                    .argument("--stable") // Bitbucket Server 5.x requires Git 2.2+, which has --stable
                    .inputHandler(new FileInputHandler(diffFile))
                    .build(new PatchIdOutputHandler())
                    .call();
        }
    }

    /**
     * Streams a specified file to stdin.
     */
    private static class FileInputHandler extends BaseInputHandler implements CommandInputHandler {

        private final File file;

        FileInputHandler(@Nonnull File file) {
            this.file = file;
        }

        @Override
        public void process(OutputStream out) {
            try (OutputStream outputStream = out) {
                IoUtils.copy(file, outputStream);
            } catch (IOException ioe) {
                throw new RuntimeException(
                        file.getAbsolutePath() + " could not be inflated and copied to stdin", ioe);
            }
        }
    }

    /**
     * Parses any number of patch IDs from {code git patch-id}.
     * <p>
     * When used with {@code git diff --binary} output, {@code patch-id} generates multiple IDs. The reason
     * for this behavior appears to be that binary diffs don't have {@code ---} lines or hunk headers.
     * <p>
     * For example, if a commit (or series of commits) modifies 4 binary files and a handful of text files,
     * {@code patch-id} will output <i>at least</i> 4 patch IDs, one for each binary file, potentially with
     * a 5th ID if any if the modified text files appears "after" the last binary file in the diff output.
     */
    private static class PatchIdOutputHandler
            extends LineReaderOutputHandler
            implements CommandOutputHandler<List<String>> {

        private List<String> patchIds;

        PatchIdOutputHandler() {
            super(StandardCharsets.UTF_8, LineReader.Mode.MODE_UNIX);
        }

        @Override
        public List<String> getOutput() {
            return patchIds;
        }

        @Override
        protected void processReader(LineReader lineReader) {
            // git patch-id output looks like:
            // 4940de4d9aa8d4e4d57450adaa146fcfd104e13c 0000000000000000000000000000000000000000
            //
            // The first SHA-1 is the patch ID, and the second is the commit ID (which will
            // always be empty with the input we provide). We're only interested in the patch
            // ID, so we split each line and only retain the patch IDs
            patchIds = lineReader.stream()
                    .map(line -> StringUtils.split(line, " ", 2)[0])
                    .collect(toImmutableList());
        }
    }

    /**
     * Takes stdout and writes it to a temporary file in the provided directory.
     * <p>
     * getOutput() returns the temporary file.
     * <p>
     * This output handler should be used in a try-with-resources block to ensure the temporary file is deleted.
     */
    private static class TmpFileOutputHandler
            extends BaseOutputHandler
            implements CommandOutputHandler<File>, AutoCloseable {

        private final File tempDir;

        private File tempFile;
        private long bytes;

        TmpFileOutputHandler(File tempDir) {
            this.tempDir = tempDir;
        }

        @Override
        public void close() {
            if (tempFile != null && tempFile.exists() && !tempFile.delete()) {
                // Attempt a best-effort deletion when the JVM shuts down instead of throwing
                tempFile.deleteOnExit();
            }
        }

        @Override
        public File getOutput() {
            return bytes > 0 ? tempFile : null;
        }

        @Override
        public void process(InputStream in) throws ProcessException {
            try (InputStream inputStream = in) {
                tempFile = File.createTempFile("patch", ".txt", tempDir);
                bytes = IoUtils.copy(inputStream, tempFile);
            } catch (IOException e) {
                if (tempFile == null) {
                    throw new ProcessException("A temp file could not be created for writing patch output", e);
                }
                throw new ProcessException("Output could not be copied to " + tempFile.getAbsolutePath(), e);
            }
        }
    }
}
