package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.audit.Channels;
import com.atlassian.bitbucket.audit.Priority;
import com.atlassian.bitbucket.event.annotation.Audited;
import com.atlassian.bitbucket.event.repository.RepositoryEvent;
import com.atlassian.bitbucket.repository.Repository;

import javax.annotation.Nonnull;

/**
 * @since 2.0
 */
@Audited(converter = AutoUnapproveSettingsChangedEventConverter.class,
        channels = {Channels.REPOSITORY_UI}, priority = Priority.HIGH)
public class AutoUnapproveSettingsChangedEvent extends RepositoryEvent {

    private final boolean enabled;

    public AutoUnapproveSettingsChangedEvent(@Nonnull Object source, @Nonnull Repository repository, boolean enabled) {
        super(source, repository);
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
