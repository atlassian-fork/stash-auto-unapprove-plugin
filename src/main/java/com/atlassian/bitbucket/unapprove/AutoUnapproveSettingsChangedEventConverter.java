package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.audit.AuditEntry;
import com.atlassian.bitbucket.audit.AuditEntryBuilder;
import com.atlassian.bitbucket.audit.AuditEntryConverter;
import com.atlassian.bitbucket.util.AuditUtils;

import javax.annotation.Nonnull;

import static com.atlassian.bitbucket.unapprove.AutoUnapproveAuditUtils.addCommonAuditDetails;

/**
 * Converts an {@link AutoUnapproveSettingsChangedEvent} to an {@link AuditEntry} for inclusion in the audit log.
 * <p>
 * This class appears in the API because it is attached to an annotation on {@link AutoUnapproveSettingsChangedEvent}.
 * It is <i>not</i> considered part of the API, however, and offers no compatibility guarantees. Plugin developers
 * should not use this class.
 */
public class AutoUnapproveSettingsChangedEventConverter implements AuditEntryConverter<AutoUnapproveSettingsChangedEvent> {

    @Nonnull
    @Override
    public AuditEntry convert(@Nonnull AutoUnapproveSettingsChangedEvent event, AuditEntryBuilder builder) {
        return addCommonAuditDetails(builder, event, event.isEnabled(), AuditUtils.toProjectAndRepositoryString(event.getRepository()))
                .repository(event.getRepository())
                .build();
    }
}
