package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.scope.Scopes;

import javax.annotation.Nonnull;

/**
 * @since 4.1
 */
public interface AutoUnapproveSettings {

    /**
     * The scope that the settings are configured for.
     *
     * @return {@link Scopes#global()} if these settings are configured at the global level,
     *         {@link Scopes#project(Project)} if these settings are configured at the project level, or
     *         {@link Scopes#repository(Repository)} if these settings are configured at the repository level
     */
    @Nonnull
    Scope getScope();

    /**
     * Whether approvals should be removed when a pull request is updated
     *
     * @return {@code true} if approvals should be removed, {@code false} otherwise
     */
    boolean isEnabled();
}
