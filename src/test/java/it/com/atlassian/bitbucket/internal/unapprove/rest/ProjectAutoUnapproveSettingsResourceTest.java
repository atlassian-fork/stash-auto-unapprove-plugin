package it.com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.test.DefaultFuncTestData;
import org.junit.After;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.removeSettingForPath;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.setEnabled;

public class ProjectAutoUnapproveSettingsResourceTest extends AbstractAutoUnapproveSettingsResourceTest {

    @After
    public void cleanup() {
        removeSettingForPath(getPath());
        setEnabled(false);
    }

    @Override
    protected String getParentPath() {
        return DefaultFuncTestData.getRestURL("auto-unapprove", "latest") + "/settings";
    }

    @Override
    protected String getParentScopeType() {
        return "GLOBAL";
    }

    @Override
    protected String getPath() {
        return DefaultFuncTestData.getRestURL("auto-unapprove", "latest") + "/projects/" + getProject1() + "/settings";
    }

    @Override
    protected String getScopeType() {
        return "PROJECT";
    }
}
