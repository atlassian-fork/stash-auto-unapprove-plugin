package it.com.atlassian.bitbucket.internal.unapprove.ui.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.bitbucket.page.BitbucketPage;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class AutoUnapproveSettingsPage extends BitbucketPage {

    private final String url;
    @ElementBy(id = "auto-unapprove-settings-form-submit")
    private PageElement saveButton;

    public AutoUnapproveSettingsPage(String url) {
        this.url = url;
    }

    public static String createUrl(String projectKey, String repositorySlug) {
        return String.format("/plugins/servlet/autounapprove/projects/%s/repos/%s", projectKey, repositorySlug);
    }

    public static String createUrl(String projectKey) {
        return String.format("/plugins/servlet/autounapprove/projects/%s", projectKey);
    }

    public static String createUrl() {
        return "/plugins/servlet/autounapprove";
    }

    public TimedQuery<String> getEnabledValue() {
        return elementFinder.find(By.cssSelector("#enabled :checked")).timed().getValue();
    }

    @Override
    public String getUrl() {
        return url;
    }

    public TimedQuery<String> getUseCustomValue() {
        return elementFinder.find(By.cssSelector(".inherit-settings-toggle :checked")).timed().getValue();
    }

    public AutoUnapproveSettingsPage save() {
        scrollToBottom();
        waitForPageLoad(saveButton::click);
        return pageBinder.bind(AutoUnapproveSettingsPage.class, url);
    }

    public void setEnabled(boolean enabled) {
        PageElement selectEnabled = elementFinder.find(By.id("enabled-" + (enabled ? "on" : "off")));
        waitUntilTrue(selectEnabled.timed().isVisible());
        selectEnabled.click();
        waitUntilTrue(selectEnabled.timed().isSelected());
    }

    public void setUseCustom(boolean useCustom) {
        PageElement selectInherit = elementFinder.find(By.id(useCustom ? "inherit-settings-selection-custom" : "inherit-settings-selection-inherit"));
        waitUntilTrue(selectInherit.timed().isVisible());
        selectInherit.click();

        if (useCustom) {
            waitUntilTrue(selectInherit.timed().isSelected());
        } else {
            PageElement areYouSureDialog = elementFinder.find(By.id("are-you-sure-dialog"));
            waitUntilTrue(areYouSureDialog.timed().isVisible());
            areYouSureDialog.find(By.className("confirm-button")).click();
            pageBinder.bind(AutoUnapproveSettingsPage.class, url);
        }
    }
}
