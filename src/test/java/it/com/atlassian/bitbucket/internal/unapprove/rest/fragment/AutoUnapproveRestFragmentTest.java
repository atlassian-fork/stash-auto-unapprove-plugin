package it.com.atlassian.bitbucket.internal.unapprove.rest.fragment;

import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.google.common.collect.ImmutableMap;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import it.com.atlassian.bitbucket.internal.unapprove.TestHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1;
import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1Repository1;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.removeSetting;
import static org.hamcrest.Matchers.equalTo;

public class AutoUnapproveRestFragmentTest extends BaseRetryingFuncTest {

    public static void assertEnabled(String projectKey, String repositorySlug, boolean enabled) {
        RestAssured.given()
                .auth().preemptive().basic(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword())
                .expect()
                .statusCode(200)
                .body("unapproveOnUpdate", equalTo(enabled))
                .when()
                .get(buildSettingsUrl(projectKey, repositorySlug));
    }

    public static void setEnabled(String projectKey, String repositorySlug, boolean enabled) {
        RestAssured.given()
                .auth().preemptive().basic(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword())
                .body(ImmutableMap.of("unapproveOnUpdate", enabled))
                .contentType(ContentType.JSON)
                .expect()
                .statusCode(200)
                .body("unapproveOnUpdate", equalTo(enabled))
                .when()
                .post(buildSettingsUrl(projectKey, repositorySlug));
    }

    @After
    public void cleanup() {
        TestHelper.setEnabled(false);
        removeSetting(getProject1());
        removeSetting(getProject1(), getProject1Repository1());
    }

    @Before
    public void setup() {
        TestHelper.setEnabled(false);
        removeSetting(getProject1());
        removeSetting(getProject1(), getProject1Repository1());
    }

    @Test
    public void testFragment() {
        String projectKey = getProject1();
        String repositorySlug = getProject1Repository1();

        //Auto-unapprove should start out disabled
        assertEnabled(projectKey, repositorySlug, false);

        //Enable auto-unapprove and verify that it's retained by requesting the settings after
        setEnabled(projectKey, repositorySlug, true);
        assertEnabled(projectKey, repositorySlug, true);

        //Disable auto-unapprove and verify that it's retained by requesting the settings after
        setEnabled(projectKey, repositorySlug, false);
        assertEnabled(projectKey, repositorySlug, false);
    }

    private static String buildSettingsUrl(String projectKey, String repositorySlug) {
        return DefaultFuncTestData.getRepositoryRestURL(projectKey, repositorySlug) + "/settings/pull-requests";
    }
}
