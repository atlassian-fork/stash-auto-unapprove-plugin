package it.com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.test.DefaultFuncTestData;
import io.restassured.RestAssured;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.setEnabled;

public class GlobalAutoUnapproveSettingsResourceTest extends AbstractAutoUnapproveSettingsResourceTest {

    @After
    public void cleanup() {
        setEnabled(false);
    }

    @Ignore("This test is not applicable at the global scope")
    @Override
    @Test
    public void testDeleteInherits() {
    }

    @Ignore("This test is not applicable at the global scope")
    @Override
    @Test
    public void testDeleteNoPermission() {
    }

    @Test
    public void testDeleteNotAllowed() {
        RestAssured.given()
                .auth().preemptive().basic(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword())
                .expect()
                .statusCode(400)
                .when()
                .delete(getPath());
    }

    @Override
    protected String getParentPath() {
        return null;
    }

    @Override
    protected String getParentScopeType() {
        return null;
    }

    @Override
    protected String getPath() {
        return DefaultFuncTestData.getRestURL("auto-unapprove", "latest") + "/settings";
    }

    @Override
    protected String getScopeType() {
        return "GLOBAL";
    }
}
