package it.com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.google.common.collect.ImmutableMap;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static org.hamcrest.Matchers.equalTo;

public final class TestHelper {

    public static void assertEnabled(String projectKey, String repositorySlug, boolean enabled, String scopeType) {
        assertEnabledForPath(enabled, scopeType, buildSettingsUrl(projectKey, repositorySlug));
    }

    public static void assertEnabled(String projectKey, boolean enabled, String scopeType) {
        assertEnabledForPath(enabled, scopeType, buildSettingsUrl(projectKey));
    }

    public static void assertEnabled(boolean enabled) {
        assertEnabledForPath(enabled, "GLOBAL", buildSettingsUrl());
    }

    public static void assertEnabledForPath(boolean enabled, String scopeType, String path) {
        RestAssured.given()
                .auth().preemptive().basic(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword())
                .expect()
                .statusCode(200)
                .body("enabled", equalTo(enabled))
                .body("scope.type", equalTo(scopeType))
                .when()
                .get(path);
    }

    public static String buildSettingsUrl() {
        return DefaultFuncTestData.getRestURL("auto-unapprove", "latest") + "/settings";
    }

    public static String buildSettingsUrl(String projectKey) {
        return DefaultFuncTestData.getRestURL("auto-unapprove", "latest") + "/projects/" + projectKey + "/settings";
    }

    public static String buildSettingsUrl(String projectKey, String repositorySlug) {
        return DefaultFuncTestData.getRestURL("auto-unapprove", "latest") + "/projects/" + projectKey + "/repos/" + repositorySlug + "/settings";
    }

    public static void removeSetting(String projectKey) {
        removeSettingForPath(buildSettingsUrl(projectKey));
    }

    public static void removeSetting(String projectKey, String repositorySlug) {
        removeSettingForPath(buildSettingsUrl(projectKey, repositorySlug));
    }

    public static void removeSettingForPath(String path) {
        RestAssured.given()
                .auth().preemptive().basic(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword())
                .expect()
                .statusCode(200)
                .when()
                .delete(path);
    }

    public static void setEnabled(boolean enabled) {
        setEnabledForPath(enabled, buildSettingsUrl());
    }

    public static void setEnabled(String projectKey, boolean enabled) {
        setEnabledForPath(enabled, buildSettingsUrl(projectKey));
    }

    public static void setEnabled(String projectKey, String repositorySlug, boolean enabled) {
        setEnabledForPath(enabled, buildSettingsUrl(projectKey, repositorySlug));
    }

    public static void setEnabledForPath(boolean enabled, String path) {
        RestAssured.given()
                .auth().preemptive().basic(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword())
                .body(ImmutableMap.of("enabled", enabled))
                .contentType(ContentType.JSON)
                .expect()
                .statusCode(200)
                .body("enabled", equalTo(enabled))
                .when()
                .post(path);
    }
}
