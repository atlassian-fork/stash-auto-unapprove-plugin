package it.com.atlassian.bitbucket.internal.unapprove.ui;

import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import it.com.atlassian.bitbucket.internal.unapprove.ui.page.AutoUnapproveSettingsPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1;
import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1Repository1;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.assertEnabled;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.removeSetting;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.setEnabled;
import static it.com.atlassian.bitbucket.internal.unapprove.ui.page.AutoUnapproveSettingsPage.createUrl;
import static org.hamcrest.Matchers.equalTo;

public class AutoUnapproveBrowserTest extends BaseRetryingFuncTest {

    protected static final BitbucketTestedProduct BITBUCKET = TestedProductFactory.create(BitbucketTestedProduct.class);

    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    @After
    public void cleanup() {
        removeSetting(getProject1(), getProject1Repository1());
        removeSetting(getProject1());
        setEnabled(false);
    }

    @Before
    public void setup() {
        BITBUCKET.visit(BitbucketLoginPage.class).login(getAdminUser(), getAdminPassword(),
                AutoUnapproveSettingsPage.class, createUrl());
        removeSetting(getProject1(), getProject1Repository1());
        removeSetting(getProject1());
        setEnabled(false);
    }

    @Test
    public void testGlobalSettings() {
        // Make sure its disabled to start with
        assertEnabled(false);

        // Visit the page and enable it
        AutoUnapproveSettingsPage page = visitSettingsPage();
        waitUntil(page.getEnabledValue(), equalTo("false"));
        page.setEnabled(true);
        page.save();

        // Revisit and check that it is still enabled
        page = visitSettingsPage();
        waitUntil(page.getEnabledValue(), equalTo("true"));

        // Disable it
        page.setEnabled(false);
        page.save();

        // Visit the page and ensure its disabled
        page = visitSettingsPage();
        waitUntil(page.getEnabledValue(), equalTo("false"));
    }

    @Test
    public void testProjectSettings() {
        // Make sure its disabled to start with
        assertEnabled(getProject1(), false, "GLOBAL");

        // Check that its inherit by default
        AutoUnapproveSettingsPage page = visitSettingsPage(getProject1());
        waitUntil(page.getUseCustomValue(), equalTo("INHERIT"));
        waitUntil(page.getEnabledValue(), equalTo("false"));

        // Visit the page and enable it
        page.setUseCustom(true);
        page.setEnabled(true);
        page.save();

        // Revisit the page and ensure its still enabled
        page = visitSettingsPage(getProject1());
        waitUntil(page.getEnabledValue(), equalTo("true"));
        waitUntil(page.getUseCustomValue(), equalTo("CUSTOM"));
        assertEnabled(getProject1(), getProject1Repository1(), true, "PROJECT");

        // Disable it
        page.setEnabled(false);
        page.save();

        // Visit the page and ensure its disabled
        page = visitSettingsPage(getProject1());
        waitUntil(page.getEnabledValue(), equalTo("false"));
        waitUntil(page.getUseCustomValue(), equalTo("CUSTOM"));
        assertEnabled(getProject1(), getProject1Repository1(), false, "PROJECT");

        // Make global enabled and then select inherit again
        setEnabled(true);
        page.setUseCustom(false);
        waitUntil(page.getUseCustomValue(), equalTo("INHERIT"));
        waitUntil(page.getEnabledValue(), equalTo("true"));

        // Revisit the page to make sure its still inherited
        page = visitSettingsPage(getProject1());
        waitUntil(page.getEnabledValue(), equalTo("true"));
        waitUntil(page.getUseCustomValue(), equalTo("INHERIT"));
        assertEnabled(getProject1(), getProject1Repository1(), true, "GLOBAL");
    }

    @Test
    public void testRepositorySettings() {
        // Make sure its disabled to start with
        assertEnabled(getProject1(), getProject1Repository1(), false, "GLOBAL");

        // Check that its inherit by default
        AutoUnapproveSettingsPage page = visitSettingsPage(getProject1(), getProject1Repository1());
        waitUntil(page.getUseCustomValue(), equalTo("INHERIT"));
        waitUntil(page.getEnabledValue(), equalTo("false"));

        // Visit the page and enable it
        page.setUseCustom(true);
        page.setEnabled(true);
        page.save();

        // Revisit the page and ensure its still enabled
        page = visitSettingsPage(getProject1(), getProject1Repository1());
        waitUntil(page.getEnabledValue(), equalTo("true"));
        waitUntil(page.getUseCustomValue(), equalTo("CUSTOM"));
        assertEnabled(getProject1(), getProject1Repository1(), true, "REPOSITORY");

        // Disable it
        page.setEnabled(false);
        page.save();

        // Visit the page and ensure its disabled
        page = visitSettingsPage(getProject1(), getProject1Repository1());
        waitUntil(page.getEnabledValue(), equalTo("false"));
        waitUntil(page.getUseCustomValue(), equalTo("CUSTOM"));
        assertEnabled(getProject1(), getProject1Repository1(), false, "REPOSITORY");

        // Make project enabled and then select inherit again
        setEnabled(getProject1(), true);
        page.setUseCustom(false);
        waitUntil(page.getUseCustomValue(), equalTo("INHERIT"));
        waitUntil(page.getEnabledValue(), equalTo("true"));

        // Revisit the page to make sure its still inherited
        page = visitSettingsPage(getProject1(), getProject1Repository1());
        waitUntil(page.getEnabledValue(), equalTo("true"));
        waitUntil(page.getUseCustomValue(), equalTo("INHERIT"));
        assertEnabled(getProject1(), getProject1Repository1(), true, "PROJECT");
    }

    private static AutoUnapproveSettingsPage visitSettingsPage() {
        return BITBUCKET.visit(AutoUnapproveSettingsPage.class, createUrl());
    }

    private static AutoUnapproveSettingsPage visitSettingsPage(String projectKey, String repositorySlug) {
        return BITBUCKET.visit(AutoUnapproveSettingsPage.class, createUrl(projectKey, repositorySlug));
    }

    private static AutoUnapproveSettingsPage visitSettingsPage(String projectKey) {
        return BITBUCKET.visit(AutoUnapproveSettingsPage.class, createUrl(projectKey));
    }
}
