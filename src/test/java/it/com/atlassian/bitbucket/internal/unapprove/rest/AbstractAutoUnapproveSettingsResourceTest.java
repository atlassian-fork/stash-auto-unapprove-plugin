package it.com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.google.common.collect.ImmutableMap;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Test;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getRegularUser;
import static com.atlassian.bitbucket.test.DefaultFuncTestData.getRegularUserPassword;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.assertEnabledForPath;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.removeSettingForPath;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.setEnabledForPath;

public abstract class AbstractAutoUnapproveSettingsResourceTest extends BaseRetryingFuncTest {

    @Test
    public void testDeleteInherits() {
        // Enable
        setEnabledForPath(true, getPath());
        assertEnabledForPath(true, getScopeType(), getPath());

        // Delete and set parent enabled
        removeSettingForPath(getPath());
        setEnabledForPath(true, getParentPath());

        assertEnabledForPath(true, getParentScopeType(), getParentPath());
        assertEnabledForPath(true, getParentScopeType(), getPath());
    }

    @Test
    public void testDeleteNoPermission() {
        RestAssured.given()
                .auth().preemptive().basic(getRegularUser(), getRegularUserPassword())
                .body(ImmutableMap.of("enabled", true))
                .contentType(ContentType.JSON)
                .expect()
                .statusCode(401)
                .when()
                .delete(getPath());
    }

    @Test
    public void testSetEnabled() {
        // Enable
        setEnabledForPath(true, getPath());
        assertEnabledForPath(true, getScopeType(), getPath());

        // Disable
        setEnabledForPath(false, getPath());
        assertEnabledForPath(false, getScopeType(), getPath());
    }

    @Test
    public void testSetNoPermission() {
        RestAssured.given()
                .auth().preemptive().basic(getRegularUser(), getRegularUserPassword())
                .body(ImmutableMap.of("enabled", true))
                .contentType(ContentType.JSON)
                .expect()
                .statusCode(401)
                .when()
                .post(getPath());
    }

    protected abstract String getParentPath();

    protected abstract String getParentScopeType();

    protected abstract String getPath();

    protected abstract String getScopeType();
}
