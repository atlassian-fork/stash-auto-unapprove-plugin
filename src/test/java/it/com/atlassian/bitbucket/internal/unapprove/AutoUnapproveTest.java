package it.com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.async.AsyncTestUtils;
import com.atlassian.bitbucket.async.WaitCondition;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.pull.PullRequestParticipantStatus;
import com.atlassian.bitbucket.test.*;
import com.google.common.collect.ImmutableMap;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import net.sf.json.JSONObject;
import org.apache.http.client.utils.URIBuilder;
import org.hamcrest.Description;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getBaseURL;
import static com.atlassian.bitbucket.test.GitTestHelper.getGitPath;
import static com.atlassian.bitbucket.test.RepositoryTestHelper.*;
import static it.com.atlassian.bitbucket.internal.unapprove.TestHelper.setEnabled;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Verifies approvals are withdrawn when pull requests are updated in certain ways.
 * <p>
 * This test <i>intentionally</i> doesn't include cases where approvals are <i>not</i> withdrawn, because, since
 * nothing changes, it's impossible to verify. (How do you differentiate between the listener taking a long time
 * to run and the listener running and not changing anything, beyond waiting a "long" time?). Instead, unit tests
 * verify both cases where approvals are and are not withdrawn, and this integration test provides end-to-end
 * verification on top of that.
 */
public class AutoUnapproveTest extends BaseRetryingFuncTest {

    @ClassRule
    public static final TemporaryFolder tempFolder = new TemporaryFolder();
    private static final String PROJECT_KEY = "AURT";
    private static final String REPOSITORY_SLUG = "unapprove";
    private static final int TIMEOUT = 5000;
    private static final String USER_AUTHOR = "unapprove-author";
    private static final String USER_REVIEWER = "unapprove-reviewer";

    @BeforeClass
    public static void setupRepositoryAndUsers() {
        getClassTestContext()
                .project(PROJECT_KEY, AutoUnapproveTest.class.getSimpleName())
                .user(USER_AUTHOR)
                .user(USER_REVIEWER);
    }

    @Before
    public void setup() {
        testContext
                .repository(PROJECT_KEY, REPOSITORY_SLUG, new ClassPathResource("git/pull-requests.zip"))
                .repositoryPermission(PROJECT_KEY, REPOSITORY_SLUG, USER_AUTHOR, Permission.REPO_WRITE)
                .repositoryPermission(PROJECT_KEY, REPOSITORY_SLUG, USER_REVIEWER, Permission.REPO_READ);
        setEnabled(PROJECT_KEY, REPOSITORY_SLUG, true);
    }

    @Test
    public void testNoUnapproveOnRescopeWhenSameDiff() throws Exception {
        PullRequestTestHelper pullRequest = createPullRequest(
                "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "refs/heads/master");
        int initialVersion = pullRequest.getVersion();
        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);

        //Introduce a new file to change the pull request's effective diff
        ammendCommit(getBaseURL(), USER_AUTHOR, USER_AUTHOR, PROJECT_KEY, REPOSITORY_SLUG,
                "branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "Bryan Turner", "bturner@atlassian.com", "Ammending the commit");
        AsyncTestUtils.waitFor(new WaitCondition() {
            @Override
            public void describeFailure(Description description) {
                description.appendText("The pull request was not rescoped within ")
                        .appendValue(TIMEOUT).appendText("ms");
            }

            @Override
            public boolean test() {
                return pullRequest.getVersion() > initialVersion;
            }
        }, TIMEOUT);
        assertThat("The pull request should be mergable, but it had the following vetoes: " +
                        pullRequest.getMergeVetoes(pullRequest.getVersion()),
                pullRequest.getMergeVetoes(pullRequest.getVersion()), empty());
    }

    @Test
    public void testUnapproveOnCrossRepositoryPullRequest() throws Exception {
        String forkSlug = REPOSITORY_SLUG + "-fork";
        testContext.forkNonPersonal(PROJECT_KEY, REPOSITORY_SLUG, PROJECT_KEY, forkSlug)
                .repositoryPermission(PROJECT_KEY, forkSlug, USER_AUTHOR, Permission.REPO_WRITE);

        PullRequestTestHelper pullRequest = this.testContext.pullRequest(
                new PullRequestTestHelper.Builder(
                        USER_AUTHOR, USER_AUTHOR, "Testing unapprovals in cross repository pull request", null,
                        PROJECT_KEY, forkSlug, "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                        PROJECT_KEY, REPOSITORY_SLUG, "refs/heads/master"
                ).reviewers(USER_REVIEWER));

        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);

        //Introduce a new file to change the pull request's effective diff
        RepositoryTestHelper.pushCommit(getBaseURL(), USER_AUTHOR, USER_AUTHOR, PROJECT_KEY, forkSlug,
                "branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "Bryan Turner", "bturner@atlassian.com", "/", "other-file.txt", "Foo",
                "Rescoping pull request to trigger unapproval");
        waitForUnapprove(pullRequest);
        assertThat(pullRequest.getMergeVetoes(pullRequest.getVersion()), IsEmptyCollection.empty());
    }

    @Test
    public void testUnapproveOnReopen() {
        PullRequestTestHelper pullRequest = createPullRequest(
                "refs/heads/branch_that_differ_by_one_file_trgt",
                "refs/heads/branch_that_differ_by_one_file_src");
        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);
        pullRequest.declineSafe();

        reopen(pullRequest);
        waitForUnapprove(pullRequest);
        assertThat(pullRequest.getMergeVetoes(pullRequest.getVersion()), IsEmptyCollection.empty());
    }

    @Test
    public void testUnapproveOnRescopeChangingDiff() throws Exception {
        PullRequestTestHelper pullRequest = createPullRequest(
                "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "refs/heads/master");
        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);

        //Introduce a new file to change the pull request's effective diff
        RepositoryTestHelper.pushCommit(getBaseURL(), USER_AUTHOR, USER_AUTHOR, PROJECT_KEY, REPOSITORY_SLUG,
                "branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "Bryan Turner", "bturner@atlassian.com", "/", "other-file.txt", "Foo",
                "Rescoping pull request to trigger unapproval");
        waitForUnapprove(pullRequest);
        assertThat(pullRequest.getMergeVetoes(pullRequest.getVersion()), IsEmptyCollection.empty());
    }

    @Test
    public void testUnapproveOnUpdateChangingTargetBranch() {
        PullRequestTestHelper pullRequest = createPullRequest(
                "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_trgt",
                "refs/heads/branch_that_has_same_text_file_modified_on_both_clean_merge_src");
        pullRequest.setStatusFor(USER_REVIEWER, PullRequestParticipantStatus.APPROVED);

        //Retarget the pull request to a branch which references a different tip commit
        retargetToMaster(pullRequest);
        waitForUnapprove(pullRequest);
        assertThat(pullRequest.getMergeVetoes(pullRequest.getVersion()), IsEmptyCollection.empty());
    }

    private static String ammendCommit(String baseUrl, String user, String password, String project, String repository,
                                       String branch, String commitAuthorName, String commitAuthorEmail, String message)
            throws IOException, URISyntaxException {

        File repDir = tempFolder.newFolder("ammend_commit_folder");

        // clone the repository into the temp folder
        ProcessTestHelper.execute(repDir, getGitPath(), "clone", "--branch", branch, "--",
                getAuthenticatedRepositoryUrl(user, password, baseUrl, project, repository), ".");

        // ammend commit and push the change
        Map<String, String> env = ImmutableMap.of(
                ENV_AUTHOR_EMAIL, commitAuthorEmail,
                ENV_AUTHOR_NAME, commitAuthorName,
                ENV_COMMITTER_EMAIL, commitAuthorEmail,
                ENV_COMMITTER_NAME, commitAuthorName);
        ProcessTestHelper.execute(repDir, env, getGitPath(), "commit", "--amend", "-m", message);
        ProcessTestHelper.execute(repDir, getGitPath(), "push", "-f", "origin");

        return ProcessTestHelper.execute(repDir, getGitPath(), "rev-parse", "HEAD").getStdOut().trim();
    }

    private static void reopen(PullRequestTestHelper pullRequest) {
        RestAssured.expect().statusCode(OK.getStatusCode())
                .log().ifValidationFails()
                .given()
                .auth().preemptive().basic(USER_AUTHOR, USER_AUTHOR)
                .body(PullRequestTestHelper.toJsonObject("version", 1))
                .contentType(ContentType.JSON)
                .when()
                .post(pullRequest.getReopenUrl());
    }

    private static void retargetToMaster(PullRequestTestHelper pullRequest) {
        JSONObject toRef = new JSONObject();
        toRef.put("id", "refs/heads/master");

        //Existing reviewers must be re-sent when updating a pull request's target branch or they
        //will be removed as part of the update
        JSONObject bodyObject = PullRequestRestTestHelper.createUpdateReviewersBody(0, USER_REVIEWER);
        bodyObject.put("toRef", toRef);

        //Re-target the pull request to master
        RestAssured.expect().statusCode(OK.getStatusCode())
                .body("version", equalTo(1))
                .log().ifValidationFails()
                .given()
                .auth().preemptive().basic(USER_AUTHOR, USER_AUTHOR)
                .body(bodyObject)
                .contentType(ContentType.JSON)
                .when()
                .put(pullRequest.getUrl());
    }

    private static void waitForUnapprove(PullRequestTestHelper pullRequest) {
        AsyncTestUtils.waitFor(new WaitCondition() {
            @Override
            public void describeFailure(Description description) {
                description.appendText("Approvals were not drawn within")
                        .appendValue(TIMEOUT)
                        .appendText("ms")
                        .appendText("Pull request object: ")
                        .appendValue(pullRequest.get().prettyPrint());
            }

            @Override
            public boolean test() {
                JsonPath response = pullRequest.get();
                return !response.getBoolean("reviewers[0].approved");
            }
        }, TIMEOUT);
    }

    private PullRequestTestHelper createPullRequest(String fromRef, String toRef) {
        return testContext.pullRequest(
                new PullRequestTestHelper.Builder(
                        USER_AUTHOR, USER_AUTHOR, "Testing unapprovals", null,
                        PROJECT_KEY, REPOSITORY_SLUG, fromRef,
                        PROJECT_KEY, REPOSITORY_SLUG, toRef
                ).reviewers(USER_REVIEWER));
    }

    private static String getAuthenticatedRepositoryUrl(String user, String password, String baseUrl, String project,
                                                        String repository) throws URISyntaxException {
        return new URIBuilder(new URI(baseUrl + "/scm/" + project + "/" + repository + ".git").normalize())
                .setUserInfo(user, password)
                .build()
                .toString();
    }
}
