package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.hook.repository.PreRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookResult;
import com.atlassian.bitbucket.hook.repository.RepositoryHookVeto;
import com.atlassian.bitbucket.hook.repository.SimplePullRequestMergeHookRequest;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestParticipant;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Repository;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PullRequestVersionMergeCheckTest {

    public static final String FROM_REF = "abc";
    @Mock
    public PreRepositoryHookContext context;
    @InjectMocks
    public PullRequestVersionMergeCheck mergeCheck;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    public PullRequest pullRequest;
    @Mock
    public Repository repository;
    @Mock
    public RescopeAnalyzer rescopeAnalyzer;
    @Mock
    public InternalAutoUnapproveService unapproveService;

    @Before
    public void setup() {
        when(unapproveService.isEnabled(pullRequest)).thenReturn(true);

        PullRequestRef fromRef = mock(PullRequestRef.class);
        when(fromRef.getLatestCommit()).thenReturn(FROM_REF);
        when(pullRequest.getFromRef()).thenReturn(fromRef);
    }

    @Test
    public void testAcceptedWhenDifHasNotChanged() {
        String oldCommit = "def";
        when(rescopeAnalyzer.isUpdated(pullRequest, oldCommit)).thenReturn(false);
        PullRequestParticipant reviewer = mock(PullRequestParticipant.class, RETURNS_DEEP_STUBS);
        when(reviewer.isApproved()).thenReturn(true);
        when(reviewer.getLastReviewedCommit()).thenReturn(oldCommit);
        when(pullRequest.getReviewers()).thenReturn(ImmutableSet.of(reviewer));

        RepositoryHookResult result = mergeCheck.preUpdate(context, new SimplePullRequestMergeHookRequest.Builder(pullRequest).build());

        verify(unapproveService, never()).withdrawApprovals(any());
        assertTrue("The hook should accept the merge", result.isAccepted());
        verify(rescopeAnalyzer).isUpdated(pullRequest, oldCommit);
    }

    @Test
    public void testAcceptedWhenDisabled() {
        when(unapproveService.isEnabled(pullRequest)).thenReturn(false);

        RepositoryHookResult result = mergeCheck.preUpdate(context, new SimplePullRequestMergeHookRequest.Builder(pullRequest).build());

        verify(rescopeAnalyzer, never()).isUpdated(any(), any());
        verify(unapproveService, never()).withdrawApprovals(any());
        assertTrue("The hook should accept the merge", result.isAccepted());
    }

    @Test
    public void testAcceptedWhenNoReviewers() {
        when(pullRequest.getReviewers()).thenReturn(Collections.emptySet());
        RepositoryHookResult result = mergeCheck.preUpdate(context, new SimplePullRequestMergeHookRequest.Builder(pullRequest).build());

        verify(unapproveService, never()).withdrawApprovals(any());
        verify(rescopeAnalyzer, never()).isUpdated(any(), any());
        assertTrue("The hook should accept the merge", result.isAccepted());
    }

    @Test
    public void testRejected() {
        // We have 4 reviewers of the PR
        // The first one hasn't approved
        PullRequestParticipant reviewer1 = mock(PullRequestParticipant.class, RETURNS_DEEP_STUBS);
        when(reviewer1.isApproved()).thenReturn(false);
        // The second one has approved, the current FROM_REF
        PullRequestParticipant reviewer2 = mock(PullRequestParticipant.class, RETURNS_DEEP_STUBS);
        when(reviewer2.isApproved()).thenReturn(true);
        when(reviewer2.getLastReviewedCommit()).thenReturn(FROM_REF);
        // The third one has approved an old commit
        String oldCommit = "def";
        when(rescopeAnalyzer.isUpdated(pullRequest, oldCommit)).thenReturn(true);
        PullRequestParticipant reviewer3 = mock(PullRequestParticipant.class, RETURNS_DEEP_STUBS);
        when(reviewer3.isApproved()).thenReturn(true);
        when(reviewer3.getLastReviewedCommit()).thenReturn(oldCommit);
        when(reviewer3.getUser().getName()).thenReturn("reviewer 3");
        // The third one has approved a different old commit
        String oldCommit2 = "ghi";
        when(rescopeAnalyzer.isUpdated(pullRequest, oldCommit2)).thenReturn(true);
        PullRequestParticipant reviewer4 = mock(PullRequestParticipant.class, RETURNS_DEEP_STUBS);
        when(reviewer4.isApproved()).thenReturn(true);
        when(reviewer4.getLastReviewedCommit()).thenReturn(oldCommit2);
        when(reviewer4.getUser().getName()).thenReturn("reviewer 4");
        when(pullRequest.getReviewers()).thenReturn(ImmutableSet.of(reviewer1, reviewer2, reviewer3, reviewer4));

        RepositoryHookResult result = mergeCheck.preUpdate(context, new SimplePullRequestMergeHookRequest.Builder(pullRequest).build());

        verify(unapproveService).withdrawApprovals(pullRequest);
        assertTrue("The hook should reject the merge", result.isRejected());
        List<String> vetoes = result.getVetoes().stream()
                .map(RepositoryHookVeto::getDetailedMessage)
                .collect(toList());
        assertThat("There should be 2 merge vetoes", vetoes, hasSize(2));
        assertThat(vetoes, containsInAnyOrder("Pull request is currently at " + FROM_REF + " but reviewer 3's latest reviewed commit is def",
                "Pull request is currently at " + FROM_REF + " but reviewer 4's latest reviewed commit is ghi"));
    }

    @Test
    public void testRescopeAnalyzerResultIsCached() {
        String commit = "def";
        when(rescopeAnalyzer.isUpdated(pullRequest, commit)).thenReturn(false);

        PullRequestParticipant reviewer1 = mock(PullRequestParticipant.class, RETURNS_DEEP_STUBS);
        when(reviewer1.isApproved()).thenReturn(true);
        when(reviewer1.getLastReviewedCommit()).thenReturn(commit);
        when(pullRequest.getReviewers()).thenReturn(ImmutableSet.of(reviewer1));

        PullRequestParticipant reviewer2 = mock(PullRequestParticipant.class, RETURNS_DEEP_STUBS);
        when(reviewer2.isApproved()).thenReturn(true);
        when(reviewer2.getLastReviewedCommit()).thenReturn(commit);

        when(pullRequest.getReviewers()).thenReturn(ImmutableSet.of(reviewer1, reviewer2));

        RepositoryHookResult result = mergeCheck.preUpdate(context, new SimplePullRequestMergeHookRequest.Builder(pullRequest).build());

        verify(unapproveService, never()).withdrawApprovals(any());
        assertTrue("The hook should accept the merge", result.isAccepted());
        verify(rescopeAnalyzer, times(1)).isUpdated(pullRequest, commit);
    }
}