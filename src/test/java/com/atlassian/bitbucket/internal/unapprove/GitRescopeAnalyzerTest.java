package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.context.DelegatingTestExecutionListener;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Branch;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.MergeCommandParameters;
import com.atlassian.bitbucket.scm.annotation.DirtiesRepo;
import com.atlassian.bitbucket.scm.annotation.Repo;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.merge.GitMergeFastForward;
import com.atlassian.bitbucket.throttle.ResourceBusyException;
import com.atlassian.bitbucket.throttle.ThrottleService;
import com.atlassian.bitbucket.throttle.Ticket;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.stash.internal.scm.git.GitAgent;
import com.atlassian.stash.internal.scm.git.GitScmConfig;
import com.atlassian.stash.internal.scm.git.fetch.ObjectFetchStrategy;
import com.atlassian.stash.internal.scm.git.merge.DefaultMergeStrategy;
import com.atlassian.stash.internal.scm.git.merge.MergeCommand;
import com.atlassian.stash.internal.scm.git.merge.MergeRequest;
import com.atlassian.stash.internal.scm.git.porcelain.GitPorcelain;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import java.util.concurrent.ExecutorService;

import static com.atlassian.bitbucket.internal.unapprove.GitRescopeAnalyzer.RESOURCE;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = GitTestConfig.class)
@TestExecutionListeners(DelegatingTestExecutionListener.class)
public class GitRescopeAnalyzerTest {

    @ClassRule
    public static final SpringClassRule CLASS_RULE = new SpringClassRule();

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();
    @Rule
    public final SpringMethodRule methodRule = new SpringMethodRule();

    @Autowired
    private GitAgent agent;
    private GitRescopeAnalyzer analyzer;
    @Autowired
    private GitCommandBuilderFactory builderFactory;
    @Autowired
    private GitScmConfig config;
    @Autowired
    private ExecutorService executorService;
    @Autowired
    private I18nService i18nService;
    @Repo("classpath:git/merge.zip")
    private Repository merges;
    @Autowired
    private GitPorcelain porcelain;
    @Repo("classpath:git/pull-requests.zip") // Has commit 017caf31eca in common with merge.zip
    private Repository pullRequests;
    @Mock
    private ThrottleService throttleService;
    @Mock
    private Ticket ticket;
    @Mock
    private AutoUnapproveConfig unapproveConfig;

    @Before
    public void setup() {
        when(throttleService.acquireTicket(eq(RESOURCE))).thenReturn(ticket);

        builderFactory = spy(builderFactory);
        analyzer = new GitRescopeAnalyzer(builderFactory, throttleService, unapproveConfig);
    }

    @Test
    public void testIsUpdated() {
        PullRequest pullRequest = mockPullRequest(merges,
                "2f3eb86189cc04ee61d2382b10755cb98e86911f",  // branch_b
                "017caf31eca7c46eb3d1800fcac431cfa7147a01"); // master
        assertTrue(analyzer.isUpdated(pullRequest, "642a6300c7f029f08da5583a54486c9cbfe03ab9")); // branch_a

        verify(throttleService).acquireTicket(eq(RESOURCE));
        verify(ticket).close();
    }

    /**
     * Verifies cross-repository diffs are generated correctly. The two "from" commits ({@code a031d5845bf} and
     * {@code 7c402d326a9}) don't exist in the {@link #merges} repository.
     */
    @Test
    public void testIsUpdatedCrossRepository() {
        PullRequest pullRequest = mockPullRequest(
                pullRequests, "a031d5845bfb8ef60f102986f98641733a3df9ba", // 101-additional-commits
                merges, "017caf31eca7c46eb3d1800fcac431cfa7147a01");      // master
        assertTrue(analyzer.isUpdated(pullRequest, "7c402d326a99fa891cd49b6e67ca492652612bac"));
    }

    @Test
    public void testIsUpdatedHandlesCommandFailures() {
        PullRequest pullRequest = mockPullRequest(merges,
                "101e612e3e71d3773568a806e76e0c9bc4381a5d",  // Not actually in the repository
                "017caf31eca7c46eb3d1800fcac431cfa7147a01"); // master
        assertTrue(analyzer.isUpdated(pullRequest, "7549846524f8aed2bd1c0249993ae1bf9d3c9998"));
    }

    @Test
    public void testIsUpdatedHandlesResourceBusy() {
        doThrow(ResourceBusyException.class).when(throttleService).acquireTicket(eq(RESOURCE));

        PullRequest pullRequest = mockPullRequest(
                pullRequests, "a031d5845bfb8ef60f102986f98641733a3df9ba", // 101-additional-commits
                merges, "017caf31eca7c46eb3d1800fcac431cfa7147a01");      // master
        assertTrue(analyzer.isUpdated(pullRequest, "7c402d326a99fa891cd49b6e67ca492652612bac"));

        verify(throttleService).acquireTicket(eq(RESOURCE));
        verifyZeroInteractions(builderFactory, ticket); // Should not run any git commands
    }

    @Test
    public void testIsUpdatedWithBinaryAndTextChanges() {
        //ddff730 and 68cbd4d both include commits that add a binary file and a text file, where the binary
        //files have identical contents but the text files don't. This produces 2 patch IDs, with the first
        //patch ID being the same and the second different
        PullRequest pullRequest = mockPullRequest(merges,
                "68cbd4d982588183413df231512a6b5746fd6638",  // branch_that_has_binary_and_text_changes_src
                "017caf31eca7c46eb3d1800fcac431cfa7147a01"); // master
        assertTrue(analyzer.isUpdated(pullRequest, "ddff73089815566e4722107f17e4bab4f3e2efba")); // branch_that_has_binary_and_text_changes_trgt
    }

    @Test
    public void testIsUpdatedWithBinaryFileAddedThenChanged() {
        //9c0bc09 adds a binary file, file.txt, and then 062fe75 modifies that file
        PullRequest pullRequest = mockPullRequest(merges,
                "062fe750c846aa5baac2ddd265003328cbb52859",  // branch_that_has_same_binary_file_modified_on_both_src
                "017caf31eca7c46eb3d1800fcac431cfa7147a01"); // master
        assertTrue(analyzer.isUpdated(pullRequest, "9c0bc09951a9c1892b26e927fa9b3fd34219078b")); // branch_that_has_same_binary_file_modified_on_both_src^1
    }

    @Test
    public void testIsUpdatedWithBinaryFileChanged() {
        //1335a5b and f6f5c91 both add binary files with the same name, file.bin, but with different contents.
        //This approximates, for example, a forced-push or rebase that rewrites a previous commit's binary file
        PullRequest pullRequest = mockPullRequest(merges,
                "f6f5c9134cc5c7282cbbd91f3ec52e4bd4671ef5",  // branch_that_has_different_binary_files_with_same_name_added_src
                "017caf31eca7c46eb3d1800fcac431cfa7147a01"); // master
        assertTrue(analyzer.isUpdated(pullRequest, "1335a5b58bcdc27804915f352c883c5fa9024e5e")); // branch_that_has_different_binary_files_with_same_name_added_trgt
    }

    /**
     * Verifies that when 2 different commits both return empty diffs, the pull request is considered updated.
     * <p>
     * This is a behavior that could really go either way. If you "review" an empty diff, and then the pull
     * request is rewritten so that different commits are used, but they also produce an empty diff, should
     * your approval be withdrawn? Since the "review" is small, it seems better to err on the side of caution
     * and consider the pull request updated.
     */
    @Test
    public void testIsUpdatedWithEmptyDiffs() {
        PullRequest pullRequest = mockPullRequest(merges,
                "298924b8c403240eaf89dcda0cce7271620ab2f6",  // branch_that_differ_by_empty_commit_trgt
                "017caf31eca7c46eb3d1800fcac431cfa7147a01"); // master
        assertTrue(analyzer.isUpdated(pullRequest, "e665fca263685e4ab64b4d88186f38af300ad38d")); // branch_that_differ_by_negating_commits_trgt
    }

    @DirtiesRepo("pullRequests")
    @Test
    public void testIsUpdatedWithMerge() {
        Branch branch = agent.resolveBranch(pullRequests, "refs/heads/branch_that_differ_by_one_file_trgt", true);
        assertNotNull("branch_that_differ_by_one_file_trgt does not exist", branch);

        Branch master = agent.resolveBranch(pullRequests, "refs/heads/master", true);
        assertNotNull("master does not exist", master);

        // Merge the target branch into the source branch. This produces a new commit but, since the
        // only modified file is not part of the pull request, the overall diff is unchanged
        ObjectFetchStrategy fetchStrategy = new ObjectFetchStrategy(agent, builderFactory,
                config, i18nService, value -> { /* No-op */ });
        ApplicationUser applicationUser = mock(ApplicationUser.class);
        when(applicationUser.getDisplayName()).thenReturn("Commit author");
        when(applicationUser.getEmailAddress()).thenReturn("commit_author@atlassian.com");
        Branch updated = new MergeCommand(executorService, agent, fetchStrategy, i18nService,
                new DefaultMergeStrategy(builderFactory, i18nService, GitMergeFastForward.OFF), porcelain,
                new MergeRequest.Builder(new MergeCommandParameters.Builder()
                        .author(applicationUser)
                        .message("Commit message")
                        .fromBranch(master.getId())
                        .toBranch(branch.getId())
                        .build())
                        .from(master)
                        .to(branch)
                        .toRepository(pullRequests)
                        .build())
                .call();
        assertNotNull("The merge completed, but an updated branch was not returned?", updated);

        PullRequest pullRequest = mockPullRequest(pullRequests,
                updated.getLatestCommit(),                   // branch_that_differ_by_one_file_trgt with new merge
                "7b86ad1a05b4259b8fa54497a8be0bd359a405bd"); // master
        assertFalse(analyzer.isUpdated(pullRequest, branch.getLatestCommit())); // Original branch tip
    }

    @Test
    public void testIsUpdatedForHgRepository() {
        Repository repository = mock(Repository.class);
        when(repository.getScmId()).thenReturn("hg");

        assertTrue(analyzer.isUpdated(mockPullRequest(repository, "currentFrom", "currentTo"), "previousFrom"));

        // Git processes should not be run for repositories from other SCMs
        verifyZeroInteractions(builderFactory, throttleService, ticket);
    }

    @Test
    public void testIsUpdatedWithUnchangedFromCommit() {
        PullRequest pullRequest = mockPullRequest(mock(Repository.class), "currentFrom", "currentTo");
        assertFalse(analyzer.isUpdated(pullRequest, "currentFrom"));

        // Git processes should not be run when the "fromRef" commit is not updated
        verifyZeroInteractions(builderFactory, throttleService, ticket);
    }

    private static PullRequestRef mockPullRequestRef(Repository repository, String latestCommit) {
        PullRequestRef pullRequestRef = mock(PullRequestRef.class);
        when(pullRequestRef.getLatestCommit()).thenReturn(latestCommit);
        when(pullRequestRef.getRepository()).thenReturn(repository);

        return pullRequestRef;
    }

    private static PullRequest mockPullRequest(Repository repository, String fromCommit, String toCommit) {
        return mockPullRequest(repository, fromCommit, repository, toCommit);
    }

    private static PullRequest mockPullRequest(Repository fromRepository, String fromCommit,
                                               Repository toRepository, String toCommit) {
        PullRequestRef fromRef = mockPullRequestRef(fromRepository, fromCommit);
        PullRequestRef toRef = mockPullRequestRef(toRepository, toCommit);

        PullRequest pullRequest = mock(PullRequest.class);
        when(pullRequest.getFromRef()).thenReturn(fromRef);
        when(pullRequest.getToRef()).thenReturn(toRef);
        when(pullRequest.isCrossRepository()).thenReturn(fromRepository != toRepository); // Reference equality

        return pullRequest;
    }
}
