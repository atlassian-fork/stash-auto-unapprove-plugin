package com.atlassian.bitbucket.internal.unapprove.web;

import com.atlassian.bitbucket.internal.unapprove.SimpleAutoUnapproveSettings;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.scope.Scopes;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AutoUnapproveSettingsServletTest {

    private static final String RESOURCE_KEY = "com.atlassian.stash.plugin.stash-auto-unapprove-plugin:auto-unapprove-settings-page-templates";
    private static final String TEMPLATE_KEY = "bitbucket.internal.autounapprove.settings.page.settingsPage";
    @Mock
    PrintWriter appendable;
    @Mock
    private AutoUnapproveService autoUnapproveService;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private NavBuilder navBuilder;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private PageBuilderService pageBuilderService;
    @Mock
    private PermissionService permissionService;
    @Mock
    private PermissionValidationService permissionValidationService;
    @Mock
    private Project project;
    @Mock
    private ProjectService projectService;
    @Mock
    private Repository repository;
    @Mock
    private RepositoryService repositoryService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @InjectMocks
    private AutoUnapproveSettingsServlet servlet;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private XsrfTokenAccessor xsrfTokenAccessor;
    @Mock
    private XsrfTokenValidator xsrfTokenValidator;

    @Before
    public void setup() throws Exception {
        when(response.getWriter()).thenReturn(appendable);

        when(project.getKey()).thenReturn("PROJECT_1");
        when(repository.getProject()).thenReturn(project);
        when(projectService.getByKey("PROJECT_1")).thenReturn(project);
        when(repositoryService.getBySlug("PROJECT_1", "rep_1")).thenReturn(repository);
        when(navBuilder.pluginServlets().path("autounapprove").buildRelative()).thenReturn("mockGlobalPath");
        when(navBuilder.pluginServlets().path("autounapprove", "projects", "PROJECT_1").buildRelative()).thenReturn("mockProjectPath");

        when(xsrfTokenValidator.getXsrfParameterName()).thenReturn("xsrfMockParameterName");
        when(xsrfTokenAccessor.getXsrfToken(request, response, true)).thenReturn("xsrfMockTokenValue");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);
    }

    @Test
    public void testDoGetGlobal() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove");
        when(autoUnapproveService.getSettings(eq(Scopes.global())))
                .thenReturn(SimpleAutoUnapproveSettings.builder(Scopes.global())
                        .enabled(true)
                        .build());

        servlet.doGet(request, response);
        ArgumentCaptor<Map<String, Object>> eventCaptor = ArgumentCaptor.forClass(Map.class);
        verify(soyTemplateRenderer).render(same(appendable), eq(RESOURCE_KEY), eq(TEMPLATE_KEY), eventCaptor.capture());
        Map<String, Object> soyParams = eventCaptor.getValue();

        assertThat(soyParams, not(hasKey("inheritUrl")));
        assertThat(soyParams, not(hasKey("repository")));
        assertThat(soyParams, not(hasKey("project")));
        assertThat(soyParams.get("useCustom"), equalTo(true));
        assertThat(soyParams.get("enabled"), equalTo(true));
        assertThat(soyParams.get("xsrfTokenName"), equalTo("xsrfMockParameterName"));
        assertThat(soyParams.get("xsrfTokenValue"), equalTo("xsrfMockTokenValue"));
    }

    @Test
    public void testDoGetProject() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1");
        when(autoUnapproveService.getSettings(eq(Scopes.project(project))))
                .thenReturn(SimpleAutoUnapproveSettings.builder(Scopes.project(project))
                        .enabled(true)
                        .build());

        servlet.doGet(request, response);
        ArgumentCaptor<Map<String, Object>> eventCaptor = ArgumentCaptor.forClass(Map.class);
        verify(soyTemplateRenderer).render(same(appendable), eq(RESOURCE_KEY), eq(TEMPLATE_KEY), eventCaptor.capture());
        Map<String, Object> soyParams = eventCaptor.getValue();

        assertThat(soyParams, not(hasKey("inheritUrl")));
        assertThat(soyParams, not(hasKey("repository")));
        assertThat(soyParams.get("project"), equalTo(project));
        assertThat(soyParams.get("useCustom"), equalTo(true));
        assertThat(soyParams.get("enabled"), equalTo(true));
        assertThat(soyParams.get("xsrfTokenName"), equalTo("xsrfMockParameterName"));
        assertThat(soyParams.get("xsrfTokenValue"), equalTo("xsrfMockTokenValue"));
    }

    @Test
    public void testDoGetProjectAsAdmin() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1");
        when(permissionService.hasGlobalPermission(Permission.ADMIN)).thenReturn(true);
        when(autoUnapproveService.getSettings(eq(Scopes.project(project))))
                .thenReturn(SimpleAutoUnapproveSettings.builder(Scopes.project(project))
                        .enabled(true)
                        .build());

        servlet.doGet(request, response);
        ArgumentCaptor<Map<String, Object>> eventCaptor = ArgumentCaptor.forClass(Map.class);
        verify(soyTemplateRenderer).render(same(appendable), eq(RESOURCE_KEY), eq(TEMPLATE_KEY), eventCaptor.capture());
        Map<String, Object> soyParams = eventCaptor.getValue();

        assertThat(soyParams.get("inheritUrl"), equalTo("mockGlobalPath"));
        assertThat(soyParams, not(hasKey("repository")));
        assertThat(soyParams.get("project"), equalTo(project));
        assertThat(soyParams.get("useCustom"), equalTo(true));
        assertThat(soyParams.get("enabled"), equalTo(true));
        assertThat(soyParams.get("xsrfTokenName"), equalTo("xsrfMockParameterName"));
        assertThat(soyParams.get("xsrfTokenValue"), equalTo("xsrfMockTokenValue"));
    }

    @Test
    public void testDoGetRepository() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1/repos/rep_1");
        when(autoUnapproveService.getSettings(eq(Scopes.repository(repository))))
                .thenReturn(SimpleAutoUnapproveSettings.builder(Scopes.repository(repository))
                        .enabled(true)
                        .build());

        servlet.doGet(request, response);
        ArgumentCaptor<Map<String, Object>> eventCaptor = ArgumentCaptor.forClass(Map.class);
        verify(soyTemplateRenderer).render(same(appendable), eq(RESOURCE_KEY), eq(TEMPLATE_KEY), eventCaptor.capture());
        Map<String, Object> soyParams = eventCaptor.getValue();

        assertThat(soyParams, not(hasKey("inheritUrl")));
        assertThat(soyParams.get("repository"), equalTo(repository));
        assertThat(soyParams.get("project"), equalTo(project));
        assertThat(soyParams.get("useCustom"), equalTo(true));
        assertThat(soyParams.get("enabled"), equalTo(true));
        assertThat(soyParams.get("xsrfTokenName"), equalTo("xsrfMockParameterName"));
        assertThat(soyParams.get("xsrfTokenValue"), equalTo("xsrfMockTokenValue"));
    }

    @Test
    public void testDoGetRepositoryAsAdmin() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1/repos/rep_1");
        when(permissionService.hasProjectPermission(eq(project), eq(Permission.PROJECT_ADMIN))).thenReturn(true);
        when(autoUnapproveService.getSettings(eq(Scopes.repository(repository))))
                .thenReturn(SimpleAutoUnapproveSettings.builder(Scopes.repository(repository))
                        .enabled(true)
                        .build());

        servlet.doGet(request, response);
        ArgumentCaptor<Map<String, Object>> eventCaptor = ArgumentCaptor.forClass(Map.class);
        verify(soyTemplateRenderer).render(same(appendable), eq(RESOURCE_KEY), eq(TEMPLATE_KEY), eventCaptor.capture());
        Map<String, Object> soyParams = eventCaptor.getValue();

        assertThat(soyParams.get("inheritUrl"), equalTo("mockProjectPath"));
        assertThat(soyParams.get("repository"), equalTo(repository));
        assertThat(soyParams.get("project"), equalTo(project));
        assertThat(soyParams.get("useCustom"), equalTo(true));
        assertThat(soyParams.get("enabled"), equalTo(true));
        assertThat(soyParams.get("xsrfTokenName"), equalTo("xsrfMockParameterName"));
        assertThat(soyParams.get("xsrfTokenValue"), equalTo("xsrfMockTokenValue"));
    }

    @Test
    public void testDoPostGlobalSettingsCustomOff() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove");
        when(request.getParameter("useCustom")).thenReturn("CUSTOM");
        when(request.getParameter("enabled")).thenReturn("false");

        servlet.doPost(request, response);

        verify(autoUnapproveService).setSettings(argThat(argument -> Scopes.global().equals(argument.getScope()) && !argument.isEnabled()));
    }

    @Test
    public void testDoPostGlobalSettingsCustomOn() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove");
        when(request.getParameter("useCustom")).thenReturn("CUSTOM");
        when(request.getParameter("enabled")).thenReturn("true");

        servlet.doPost(request, response);

        verify(autoUnapproveService).setSettings(argThat(argument -> Scopes.global().equals(argument.getScope()) && argument.isEnabled()));
    }

    @Test
    public void testDoPostProjectSettingsCustomOff() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1");
        when(request.getParameter("useCustom")).thenReturn("CUSTOM");
        when(request.getParameter("enabled")).thenReturn("false");

        servlet.doPost(request, response);

        verify(autoUnapproveService).setSettings(argThat(argument -> Scopes.project(project).equals(argument.getScope()) && !argument.isEnabled()));
    }

    @Test
    public void testDoPostProjectSettingsCustomOn() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1");
        when(request.getParameter("useCustom")).thenReturn("CUSTOM");
        when(request.getParameter("enabled")).thenReturn("true");

        servlet.doPost(request, response);

        verify(autoUnapproveService).setSettings(argThat(argument -> Scopes.project(project).equals(argument.getScope()) && argument.isEnabled()));
    }

    @Test
    public void testDoPostProjectSettingsInherit() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1");
        when(request.getParameter("useCustom")).thenReturn("INHERIT");

        servlet.doPost(request, response);

        verify(autoUnapproveService).removeSettings(Scopes.project(project));
    }

    @Test
    public void testDoPostRepositorySettingsCustomOff() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1/repos/rep_1");
        when(request.getParameter("useCustom")).thenReturn("CUSTOM");
        when(request.getParameter("enabled")).thenReturn("false");

        servlet.doPost(request, response);

        verify(autoUnapproveService).setSettings(argThat(argument -> Scopes.repository(repository).equals(argument.getScope()) && !argument.isEnabled()));
    }

    @Test
    public void testDoPostRepositorySettingsCustomOn() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1/repos/rep_1");
        when(request.getParameter("useCustom")).thenReturn("CUSTOM");
        when(request.getParameter("enabled")).thenReturn("true");

        servlet.doPost(request, response);

        verify(autoUnapproveService).setSettings(argThat(argument -> Scopes.repository(repository).equals(argument.getScope()) && argument.isEnabled()));
    }

    @Test
    public void testDoPostRepositorySettingsInherit() throws Exception {
        when(request.getPathInfo()).thenReturn("/autounapprove/projects/PROJECT_1/repos/rep_1");
        when(request.getParameter("useCustom")).thenReturn("INHERIT");

        servlet.doPost(request, response);

        verify(autoUnapproveService).removeSettings(Scopes.repository(repository));
    }
}
