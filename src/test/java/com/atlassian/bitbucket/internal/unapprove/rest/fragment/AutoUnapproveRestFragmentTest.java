package com.atlassian.bitbucket.internal.unapprove.rest.fragment;

import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.i18n.SimpleI18nService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.rest.fragment.RestFragmentContext;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bitbucket.internal.unapprove.rest.fragment.AutoUnapproveRestFragment.AUTO_UNAPPROVE_KEY;
import static java.util.Collections.emptyMap;
import static java.util.Optional.ofNullable;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AutoUnapproveRestFragmentTest {

    @InjectMocks
    private AutoUnapproveRestFragment fragment;
    @Spy
    @SuppressWarnings("unused")
    private I18nService i18nService = new SimpleI18nService();
    @Mock
    private Repository repository;
    private Map<String, Object> requestContext;
    @Mock
    private AutoUnapproveService unapproveService;

    @Before
    public void setup() {
        requestContext = ImmutableMap.of("repository", repository);
    }

    @Test
    public void testExecuteForGET() {
        when(unapproveService.isEnabled(same(repository))).thenReturn(true);

        Map<String, Object> result = fragment.execute(mockFragmentContext("GET"), requestContext);
        assertThat(result, hasEntry(AUTO_UNAPPROVE_KEY, Boolean.TRUE));

        verify(unapproveService).isEnabled(same(repository));
    }

    @Test
    public void testExecuteForHEAD() {
        Map<String, Object> result = fragment.execute(mockFragmentContext("HEAD"), requestContext);
        assertThat(result, hasEntry(AUTO_UNAPPROVE_KEY, "bitbucket.web.autounapprove.rest.method.unsupported"));

        verifyZeroInteractions(unapproveService);
    }

    @Test
    public void testExecuteForPOST() {
        Map<String, Object> result = fragment.execute(mockFragmentContext("POST", Boolean.FALSE), requestContext);
        assertThat(result, hasEntry(AUTO_UNAPPROVE_KEY, Boolean.FALSE));

        verify(unapproveService).isEnabled(same(repository));
        verify(unapproveService).setEnabled(same(repository), eq(false));
    }

    @Test
    public void testExecuteForPOSTWithoutSetting() {
        Map<String, Object> result = fragment.execute(mockFragmentContext("POST"), requestContext);
        assertThat(result, hasEntry(AUTO_UNAPPROVE_KEY, Boolean.FALSE));

        verify(unapproveService).isEnabled(same(repository));
        verify(unapproveService, never()).setEnabled(any(), anyBoolean());
    }

    @Test
    public void testValidateForGET() {
        RestFragmentContext context = mockFragmentContext("GET", Boolean.TRUE);

        assertEquals(ImmutableMap.of(), fragment.validate(context, emptyMap()));

        verify(context, never()).getBodyProperty(anyString());
    }

    @Test
    public void testValidateForPOSTWithInvalidSetting() {
        RestFragmentContext context = mockFragmentContext("POST", "false");

        Map<String, Object> errors = fragment.validate(context, emptyMap());
        assertFalse(errors.isEmpty());
        assertThat(errors, allOf(
                hasEntry("context", AUTO_UNAPPROVE_KEY),
                hasEntry("message", "bitbucket.web.autounapprove.rest.invalid"),
                hasEntry(equalTo("exceptionName"), nullValue())));
    }

    @Test
    public void testValidateForPOSTWithValidSetting() {
        RestFragmentContext context = mockFragmentContext("POST", Boolean.TRUE);

        assertEquals(ImmutableMap.of(), fragment.validate(context, emptyMap()));
    }

    @Test
    public void testValidateForPOSTWithoutSetting() {
        RestFragmentContext context = mockFragmentContext("POST");

        assertEquals(ImmutableMap.of(), fragment.validate(context, emptyMap()));
    }

    private static RestFragmentContext mockFragmentContext(String method) {
        return mockFragmentContext(method, null);
    }

    private static RestFragmentContext mockFragmentContext(String method, Object value) {
        RestFragmentContext context = mock(RestFragmentContext.class);
        when(context.getMethod()).thenReturn(method);
        when(context.getBodyProperty(eq(AUTO_UNAPPROVE_KEY))).thenReturn(ofNullable(value));

        return context;
    }
}
