package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.junit.TemporaryDirectory;
import com.atlassian.bitbucket.server.StorageService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAutoUnapproveConfigTest {

    @Rule
    public final TemporaryDirectory folder = new TemporaryDirectory();

    private DefaultAutoUnapproveConfig config;
    @Mock
    private StorageService storageService;

    @Before
    public void setup() {
        when(storageService.getTempDir()).thenReturn(folder.getRoot());

        config = new DefaultAutoUnapproveConfig(storageService);
    }

    @Test
    public void testGetTempDir() {
        File tempDir = config.getTempDir();
        assertTrue(tempDir.isDirectory());
        assertEquals("unapprove", tempDir.getName());
        assertEquals(new File(folder.getRoot().toFile(), "unapprove").getAbsolutePath(), tempDir.getAbsolutePath());
    }
}
